#package used to read jpg-images
from PIL import Image


def writefile(name,header,pixels):
    """Write an image file.

    Arguments:
    name -- the name of the new file
    header -- the image header
    pixels -- a list of pixels (consists of tuples for rgb or single integers for b/w)
    """
    outfile = open(name, 'w')
    outfile.write(header[0] + '\n' + header[1] + ' ' + header[2] + '\n')
    if len(header) > 3:
        outfile.write(header[3] + '\n')
    if isinstance(pixels[0], int):
        for p in pixels:
            outfile.write(str(p)+' ')
    else:
        for p in pixels:
            outfile.write(str(p[0])+' '+str(p[1])+' '+str(p[2])+' ')
    outfile.close()
    

# read and process the raw image data
data = open("tulip.ppm",'r')
raw_data=data.read()
splits=raw_data.split(None, 4)
raw_pixel_data = [int(x) for x in splits[4].split()]

# create tupels for every rgb pixel
pixels = list(zip(*[iter(raw_pixel_data)]*3))


### assignment 1 ###
new_pixels = [(p[0],p[1],0) for p in pixels]
writefile('1.1.ppm',splits[:4],new_pixels)


### assignment 2 ###

# set red and green to 0, leave blue
new_pixels = [(0,0,p[2]) if p[2] > p[0] and p[2]>p[1] else (p[0],p[1],p[2]) for p in pixels]
writefile('1.2.ppm',splits[:4],new_pixels)

# (alternative version) set red and green to 0, blue to 255
new_pixels = [(0,0,255) if p[2] > p[0] and p[2]>p[1] else (p[0],p[1],p[2]) for p in pixels]
writefile('Alternativen/1.2.1.ppm',splits[:4],new_pixels)


### assignment 3 ###

# compare with >= (created image corresponds to the solution image)
new_pixels = [(255,p[1],p[2]) if p[0] >= p[1] and p[0]>=p[2] else (p[0],255,p[2]) if p[1] >= p[0] and p[1]>=p[2] \
    else (p[0],p[1],255) if p[2] >= p[0] and p[2]>=p[1] else (p[0],p[1],p[2]) for p in pixels]
writefile('1.3.ppm',splits[:4],new_pixels)

# compare with >, don't do anything if there is no clear biggest value
new_pixels = [(255,p[1],p[2]) if p[0] > p[1] and p[0]>p[2] else (p[0],255,p[2]) if p[1] > p[0] and p[1]>p[2] \
    else (p[0],p[1],255) if p[2] > p[0] and p[2]>p[1] else (p[0],p[1],p[2]) for p in pixels]
writefile('Alternativen/1.3.1.ppm',splits[:4],new_pixels)

# if there are multiple max values, set them all to 255
new_pixels = [(255,255,255)if p[0]==p[1]and p[1]==p[2]else (255,255,p[2])if p[0]==p[1]and p[1]>p[2] \
    else (255,p[1],255)if p[0]==p[2]and p[0]>p[1]else (p[0],255,255)if p[2]==p[1]and p[1]>p[0] \
        else(255,p[1],p[2]) if p[0] > p[1] and p[0]>p[2] else (p[0],255,p[2]) if p[1] > p[0] and p[1]>p[2] \
            else (p[0],p[1],255) if p[2] > p[0] and p[2]>p[1] else (p[0],p[1],p[2]) for p in pixels]
writefile('Alternativen/1.3.2.ppm',splits[:4],new_pixels)


### assignment 4 ###
new_pixels = [(int(p[0]/51),int(p[1]/51),int(p[2]/51)) for p in pixels]
writefile('1.4.ppm',['P3','1024','768','5'],new_pixels)


### assignment 5 ###
new_pixels = [int(0.299*p[0]+0.587*p[1]+0.114*p[2]) for p in pixels]
writefile('1.5.pgm',['P2','1024','768','255'],new_pixels)


### assignment 6 ###
new_pixels = [1 if i / 100 > 29 and i / 100 < 70 and i % 100 > 29 and i % 100 < 70 else 0 for i in range(10000)]
writefile('1.6.pbm',['P1','100','100'],new_pixels)


### bonus assignment ###
im = Image.open('Tulip.jpg','r')
jpg_data = list(im.getdata())
new_pixels = [(p[0],p[1],p[2]) for p in jpg_data]
writefile('1.zusatz.ppm',splits[:4],new_pixels)