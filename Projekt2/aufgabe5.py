# generate wav file containing sine waves
# FB36 - 20120617
import math, wave, array


def add_freq_for_sec (data, freq, sec, numSamples2):
    """
    Diese funktion hat die Aufgabe An die übergebene Liste für eine Entsprechente zeit die übergebene Frequenz anzuhängen
    :param data: Liste an die die daten agehänt werden sollen
    :param freq: Frequenz die genutzt werden soll
    :param sec: zeit die genutztwerden soll
    :param numSamples2: vorab schon angehängte daten
    :return: insgesamt angehänte daten
    """
    samplerate = 44100
    numsamplespercyc = int(samplerate / freq)
    numsamples = int(samplerate * sec)
    for i in range(numsamples):
        sample = 32767 * float(volume) / 100
        sample *= math.sin(math.pi * 2 * (i % numsamplespercyc) / numsamplespercyc)
        data.append(int(sample))
    return numSamples2+numSamples

# Aufgabe 5 Begin
# Festlegung algemeiner benötigter Variablen
duration = 1 # seconds
freqc1 = 264
freqd = 297
freqe = 330
freqf = 352
freqg = 396
freqa = 440
freqh = 495
freqc2 = 528
sampleRate = 44100
numSamples = 0
volume = 100 # percent
data = array.array('h') # signed short integer (-32768 to 32767) data
numChan = 1 # of channels (1: mono, 2: stereo)
dataSize = 2 # 2 bytes because of using signed short integers => bit depth = 16

# Abspeicherung der Noten von Hänschen klein nach noten von http://www.franzdorfer.com/hh
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqf, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqc1, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqf, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.5, numSamples)

numSamples = add_freq_for_sec(data, freqf, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqc1, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqc1, 1, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqf, 0.5, numSamples)

numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqf, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.5, numSamples)

numSamples = add_freq_for_sec(data, freqf, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqd, 0.5, numSamples)
numSamples = add_freq_for_sec(data, freqc1, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqe, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqg, 0.25, numSamples)
numSamples = add_freq_for_sec(data, freqc1, 1, numSamples)


f = wave.open('wav/aufgabe5.wav', 'w')

# Abspeicherung der Parameter in der Datei
f.setparams((numChan, dataSize, sampleRate, numSamples, "NONE", "Uncompressed"))

# Abspeicherung der Audiodaten in die Datei
f.writeframes(data.tostring())
f.close()

# Aufgabe 5 end