import math
import wave
import struct

# Festlegung algemeiner benötigter Variablen
freq = 440.0
freq2 = 264.0
freqc1 = 264.0
freqd = 297.0
freqe = 330.0
freqf = 352.0
freqg = 396.0
freqa = 440.0
freqh = 495.0
freqc2 = 528.0
data_size = 80000
fname1 = "wav/aufgabe3.wav"
fname2 = "wav/aufgabe4.wav"
frate = 11025.0  # framerate as a float
amp = 64000.0     # multiplier for amplitude

# Aufgabe 3 start
sine_list_x1 = []
i=0;
# Generirung Daten für die Tonleiter in mono
for x in range(data_size):
    if i<10000:
        sine_list_x1.append(math.sin(2 * math.pi * freqc1 * (x / frate)))
    elif i<20000:
        sine_list_x1.append(math.sin(2 * math.pi * freqd * (x / frate)))
    elif i<30000:
        sine_list_x1.append(math.sin(2 * math.pi * freqe * (x / frate)))
    elif i< 40000:
        sine_list_x1.append(math.sin(2 * math.pi * freqf * (x / frate)))
    elif i<50000:
        sine_list_x1.append(math.sin(2 * math.pi * freqg * (x / frate)))
    elif i<60000:
        sine_list_x1.append(math.sin(2 * math.pi * freqa * (x / frate)))
    elif i<70000:
        sine_list_x1.append(math.sin(2 * math.pi * freqh * (x / frate)))
    else:
        sine_list_x1.append(math.sin(2 * math.pi * freqc2 * (x / frate)))
    i = i + 1

wav_file = wave.open(fname1, "w")

# Festlegung der Parameter für die Datei
nchannels = 1
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s in sine_list_x1:
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))

wav_file.close()
# Aufgabe 3 end

# Aufgabe 4 start
sine_list_x2 = []
sine_list_y2 = []
i=0;
# Generirung Daten für die Tonleiter in stereo
for x in range(data_size):
    if i<10000:
        sine_list_x2.append(math.sin(2 * math.pi * freqc1 * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqc2 * (x / frate)))
    elif i<20000:
        sine_list_x2.append(math.sin(2 * math.pi * freqd * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqh * (x / frate)))
    elif i<30000:
        sine_list_x2.append(math.sin(2 * math.pi * freqe * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqa * (x / frate)))
    elif i< 40000:
        sine_list_x2.append(math.sin(2 * math.pi * freqf * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqg * (x / frate)))
    elif i<50000:
        sine_list_x2.append(math.sin(2 * math.pi * freqg * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqf * (x / frate)))
    elif i<60000:
        sine_list_x2.append(math.sin(2 * math.pi * freqa * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqe * (x / frate)))
    elif i<70000:
        sine_list_x2.append(math.sin(2 * math.pi * freqh * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqd * (x / frate)))
    else:
        sine_list_x2.append(math.sin(2 * math.pi * freqc2 * (x / frate)))
        sine_list_y2.append(math.sin(2 * math.pi * freqc1 * (x / frate)))
    i = i + 1

wav_file = wave.open(fname2, "w")

# Festlegung der Parameter für die Datei
nchannels = 2
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s, t in zip(sine_list_x2, sine_list_y2):
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))
    wav_file.writeframes(struct.pack('h', int(t*amp/2)))

wav_file.close()
# Aufgabe 4 end