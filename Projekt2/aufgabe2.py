import math
import wave
import struct

# Festlegung algemeiner benötigter Variablen
freqa = 440.0
data_size = 80000
fname1 = "wav/aufgabe2mono.wav"
fname2 = "wav/aufgabe2stereo.wav"
frate = 11025.0  # framerate as a float
amp = 64000.0     # multiplier for amplitude

# Generirung Daten für kammerton A in Mono
sine_list_x1 = []
for x in range(data_size):
    sine_list_x1.append(math.sin(2 * math.pi * freqa * (x / frate)))
wav_file = wave.open(fname1, "w")

# Festlegung der Parameter für die Datei
nchannels = 1
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s in sine_list_x1:
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))

wav_file.close()


# Generirung Daten für kammerton A in Stereo mit abwegselnden abspielseiten
sine_list_x2 = []
sine_list_y2 = []
i=0;
for x in range(data_size):
    if i<10000:
        sine_list_x2.append(math.sin(2 * math.pi * freqa * (x / frate)))
        sine_list_y2.append(0)
    elif i<20000:
        sine_list_x2.append(0)
        sine_list_y2.append(math.sin(2 * math.pi * freqa * (x / frate)))
    elif i<30000:
        sine_list_x2.append(math.sin(2 * math.pi * freqa * (x / frate)))
        sine_list_y2.append(0)
    elif i< 40000:
        sine_list_x2.append(0)
        sine_list_y2.append(math.sin(2 * math.pi * freqa * (x / frate)))
    elif i<50000:
        sine_list_x2.append(math.sin(2 * math.pi * freqa * (x / frate)))
        sine_list_y2.append(0)
    elif i<60000:
        sine_list_x2.append(0)
        sine_list_y2.append(math.sin(2 * math.pi * freqa * (x / frate)))
    elif i<70000:
        sine_list_x2.append(math.sin(2 * math.pi * freqa * (x / frate)))
        sine_list_y2.append(0)
    else:
        sine_list_x2.append(0)
        sine_list_y2.append(math.sin(2 * math.pi * freqa * (x / frate)))
    i = i + 1

wav_file = wave.open(fname2, "w")

# Festlegung der Parameter für die Datei
nchannels = 2
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s, t in zip(sine_list_x2, sine_list_y2):
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))
    wav_file.writeframes(struct.pack('h', int(t*amp/2)))

wav_file.close()