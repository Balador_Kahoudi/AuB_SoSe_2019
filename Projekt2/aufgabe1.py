import struct
import io


file = io.open('wav/trinken.wav', 'rb')

# read in binary data and convert to usable format (see: https://docs.python.org/3.7/library/struct.html)
# struct.unpack gets a format string in the first parameter
# the meaning of the symbols can be found on the link above.
# It can be read as:
# '<' means little endian
# '4s' means interpret the following 4 bytes as characters
# 'I' means interpret the following 4 bytes as unsigned int
# 'H' means interpret the following 2 byte as unsigned short
chunkdescriptor, chunksize, fformat, fmtsubchunk, subchunk1size, audioformat, channels, samplerate, byterate, \
blockalign, bitspersample, datasubchunk, subchunk2size = struct.unpack('<4sI4s4sIHHIIHH4sI', file.read(44))

# print the properties
print('Riff: %s \nRIFF-length: %i \nformat: %s \nfmt: %s \nFormat-size: %s \nAudiotype: %i \nChannels: %i '
      '\nSample Rate: %i \nByterate: %i \nBits per sample: %i \nBlockAlign: %i \nID: %s \nData size: %i \nSignal Duration: %f'
      % (chunkdescriptor, chunksize, fformat, fmtsubchunk, subchunk1size, audioformat, channels, samplerate, byterate,
         bitspersample, blockalign, datasubchunk, subchunk2size, subchunk2size/byterate))

file.close()
