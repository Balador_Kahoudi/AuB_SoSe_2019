import math
import wave
import struct


def freqpertiminlist(freqtoset, timeforfreq, starttime, into, fratetouse):
    """
    Diese Funktion hat den Zweck Eine festgelegte frequenz für einen übergebenen Zeitraum an die Liste welche
    anschließend gespeichert werden soll zu speichern
    :param freqtoset: Festlegung der Frequenz welche genutzt werden soll
    :param timeforfreq: Dauer der abzuspeichernden Daten
    :param starttime: festlegung des zeitpunktes ab dem daten gespeichert werden sollen
    :param into: übergabe der Liste in die Gespeichert werden soll
    :param fratetouse: festlegung der zu nutzenden Framerate
    :return: insgesamt bisher gesetzte Zeit
    """
    # im falle das die zu setzende Frequenz 0 ist also lkein tonn gesetzt werden soll muss eine 0 angehängt werden
    if freqtoset == 0:
        for x in range(timeforfreq):
            into.append(0)
    # Andernfals wird eine entspechent bearbeitete Frequenz angehängt
    else:
        for x in range(timeforfreq):
            y = x+starttime
            into.append(math.sin(2 * math.pi * freqtoset * (y / fratetouse)))
    return starttime+timeforfreq


def notelisttolist(notenliste, listtoinsert, frate2):
    """
    Diese Funktion hat die aufgabe eine übergebenen Liste als noten zu Interpretiren und die entsprechenden
    Frequenzen in eine Liste abzuspeichern
    :param notenliste: Liste mit den zu setzenden Noten
    :param listtoinsert: Liste an die die Noten angehängt werden sollen
    :param frate2: zu Nutzende Framrate
    :return: Gesamte dauer der gesetzten Noten
    """
    atime = 0
    # Auswahl der jeweisls zu der note zugeordneten Frequenz
    for row in notenliste:
        if row[0] == 'c1':
            atime = freqpertiminlist(264.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'd':
            atime = freqpertiminlist(297.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'e':
            atime = freqpertiminlist(330.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'f':
            atime = freqpertiminlist(352.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'g':
            atime = freqpertiminlist(396.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'a':
            atime = freqpertiminlist(444.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'h':
            atime = freqpertiminlist(495.0, int(row[1]*10000), atime, listtoinsert, frate2)
        elif row[0] == 'c2':
            atime = freqpertiminlist(528.0, int(row[1]*10000), atime, listtoinsert, frate2)
        else:
            atime = freqpertiminlist(0, int(row[1]*10000), atime, listtoinsert, frate2)
    return atime

# Noten listen für Tonleiter
notetoneleiterup = [
    ['c1', 1.0],
    ['d', 1.0],
    ['e', 1.0],
    ['f', 1.0],
    ['g', 1.0],
    ['a', 1.0],
    ['h', 1.0],
    ['c2', 1.0]
]
notetoneleiterdown = [
    ['c2', 1.0],
    ['h', 1.0],
    ['a', 1.0],
    ['g', 1.0],
    ['f', 1.0],
    ['e', 1.0],
    ['d', 1.0],
    ['c1', 1.0]
]

# Festlegung algemeiner benötigter Variablen
fname1 = "wav/aufgabe3bonus.wav"
fname2 = "wav/aufgabe4bonus.wav"
frate = 11025.0  # framerate as a float
amp = 64000.0     # multiplier for amplitude

# Aufgabe 3 start
sine_list_x1 = []
data_size = notelisttolist(notetoneleiterup, sine_list_x1, frate)
wav_file = wave.open(fname1, "w")

# Festlegung der Parameter für die Datei
nchannels = 1
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s in sine_list_x1:
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))

wav_file.close()
# Aufgabe 3 end
# Aufgabe 4 start
sine_list_x2 = []
sine_list_y2 = []
data_size = notelisttolist(notetoneleiterup, sine_list_x2, frate)
data_size = notelisttolist(notetoneleiterdown, sine_list_y2, frate)
wav_file = wave.open(fname2, "w")

# Festlegung der Parameter für die Datei
nchannels = 2
sampwidth = 2
framerate = int(frate)
nframes = data_size
comptype = "NONE"
compname = "not compressed"

# Abspeicherung der Parameter in der Datei
wav_file.setparams((nchannels, sampwidth, framerate, nframes,
    comptype, compname))

for s, t in zip(sine_list_x2, sine_list_y2):
    # Abspeicherung der Audiodaten in die Datei
    wav_file.writeframes(struct.pack('h', int(s*amp/2)))
    wav_file.writeframes(struct.pack('h', int(t*amp/2)))

wav_file.close()
# Aufgabe 4 end